
Code review of Master branch.

1. What is done well?

    -- >hasellfree local setup
    --> having components in lib makes application much more modular and can be used in other applications. 
    --> using Hapi server 


2. What would you change?

    --> would avoid createComponent test cases
    --> implement Ondestroy method to unsubscribe observables

3. Are there any code smells or problematic implementations?

    --> In chat compoent, ngIf was binding to a value which as in component's scope
    --> code wasnt saving symbol in store. Commited that change.
    --> task 2  was almost complete except giving contenxt of the object by using bind(this). fixed that in another problematic.
    --> test cases failing in master branch. 
    --> API wasnt available for date range selection (https://iexcloud.io/docs/api/#historical-prices)
